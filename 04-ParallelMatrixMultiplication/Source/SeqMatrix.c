/***********************************************************************************/
//	CS789 - Parallel Programming
//	Assignment #4 - System of Equations (Sequential)
//	Sachintha Gurudeniya
//	11/4/2015
/***********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdarg.h>
#include <sys/time.h>

void getSecsUsecs(long long l, long *secs, long *usecs) {
	*secs = l/1000000;
	*usecs = l%1000000;
}

void printTime(int rank, struct timeval t1, struct timeval t2) {
	long long l;
	long secs, usecs;

	l = t2.tv_sec*1000000+t2.tv_usec-(t1.tv_sec*1000000+t1.tv_usec);

	secs = l/1000000;
	usecs = l%1000000;

	printf("Rank = %d: (Start = %ld.%d -> End = %ld.%d (Time = %ld.%ld)\n", rank, t1.tv_sec, t1.tv_usec, t2.tv_sec, t2.tv_usec, secs,usecs);
}

long long getTime(int rank, struct timeval t1, struct timeval t2) {
	long long l;

	l = t2.tv_sec*1000000+t2.tv_usec-(t1.tv_sec*1000000+t1.tv_usec);

	return l;
}

void initMatrix(int** MATRIX, int size) {
	int count = 0;
	int i, j;
	for(i = 0; i < size; i++) {
		for(j = 0; j < size; j++) {
			count++;
			MATRIX[i][j] = count;
			MATRIX[i][j] = count;
		}
	}
}

void printMatrix(int** MATRIX, int size) {
	int i, j;
	for(i = 0; i < size; i++) {
		for(j = 0; j < size; j++) {
			printf("%d\t", MATRIX[i][j]);
		}
		printf("\n");
	}
}

void CreateMatrix(char *fileName, int size) {
	int i, j;
	FILE *f = fopen(fileName, "w");

	srand(15);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			fprintf(f, "%d\t", rand() % 3);
		}
		fprintf(f, "\n");
	}

	fclose(f);
}

int main(int argc, char *argv[]) {
	/*------ Time Start ------*/
	long totalU = 0;
	long totalS = 0;
	long long totalTime = 0;
	struct timeval t1, t2;

	gettimeofday(&t1, NULL);
	/*------------------------*/

	int M;
	int** A;
	int** B;
	int** C;
	int i, j, n, count,ret;
	FILE *fA, *fB;

	if (argc != 2) {
		printf("Usage: Matrix <M>\n");
		printf("\t M \t = Matrix Size\n");
		printf("%d args supplied\n", argc);
		exit(0);
	}

	/* Get variables from commando line */
	M = atoi(argv[1]);
	CreateMatrix("A.txt", M);
	CreateMatrix("B.txt", M);
	
	A = (int**)malloc(sizeof(int*) * M);
	for (i = 0; i < M; i++) {
		A[i] = (int*)malloc(sizeof(int) * M);
	}
	B = (int**)malloc(sizeof(int*) * M);
	for (i = 0; i < M; i++) {
		B[i] = (int*)malloc(sizeof(int) * M);
	}
	C = (int**)malloc(sizeof(int*) * M);
	for (i = 0; i < M; i++) {
		C[i] = (int*)malloc(sizeof(int) * M);
	}

	fA = fopen("A.txt", "r");
	for(i = 0; i < M; i++) {
		for(j = 0; j < M; j++) {
			ret = fscanf(fA, "%d", &A[i][j]);
		}
	}
	fclose(fA);

	fB = fopen("B.txt", "r");
	for(i = 0; i < M; i++) {
		for(j = 0; j < M; j++) {
			ret = fscanf(fB, "%d", &B[i][j]);
		}
	}
	fclose(fB);

	for (i = 0; i < M; i++) {
		for (j = 0; j < M; j++) {
			C[i][j] = 0;
			for (n = 0; n < M; n++) {
				C[i][j] = C[i][j] + A[i][n] * B[n][j];
			}
		}
	}

	printf("----------- MATRIX A -----------\n");
	printMatrix(A, M);
	printf("----------- MATRIX B -----------\n");
	printMatrix(B, M);
	printf("\n");
	printf("----------- MATRIX C -----------\n");
	printMatrix(C, M);

	free(A);
	free(B);
	free(C);

	gettimeofday(&t2, NULL);
	totalTime = getTime(0, t1, t2);
	getSecsUsecs(totalTime, &totalS, &totalU);
	printf("Rank = %d, TIME = %ld.%ld\n", 0, totalS, totalU);

	return 0;
}
