#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

struct complex {
	float real;
	float imag;
};

void printTime(int rank, struct timeval t1, struct timeval t2) {
	long long l;
	long secs, usecs;

	l = t2.tv_sec*1000000+t2.tv_usec-(t1.tv_sec*1000000+t1.tv_usec);

	secs = l/1000000;
	usecs = l%1000000;

	printf("%d: (Start = %ld:%d -> End = %ld:%d (Time = %ld:%ld)\n", rank, t1.tv_sec, t1.tv_usec, t2.tv_sec, t2.tv_usec, secs,usecs);
}

int cal_pixel(struct complex c) {
	int count, max;
	struct complex z;
	float temp, lengthsq;

	max = 256;
	z.real = 0;
	z.imag = 0;
	count =0;
	
	do {
		float realSq = z.real * z.real;
		float imagSq = z.imag * z.imag;
		z.imag = 2 * z.real * z.imag + c.imag;
		z.real = realSq - imagSq + c.real;
		lengthsq = realSq + imagSq;
		count++;
	} while ((lengthsq < 4.0) && (count < max));
	
	return count;
}

int main(int argc, char *argv[]) {

	struct timeval t1, t2;
	gettimeofday(&t1,NULL);

	int disp_width, disp_height;
	float real_min, real_max, imag_min, imag_max, scale_real, scale_imag;
	FILE *f;
	struct complex c;
	int x,y,i;
	char str[256];

	int map[3][257];

	if (argc != 9) {
		printf("Usage:\n Mandelbrot width height real-min real-max imag-min imag-max mapfile outfile\n");
		exit(1);
	}

	/* Decode arguments */
	disp_width  = atoi(argv[1]);
	disp_height = atoi(argv[2]);

	real_min = atof(argv[3]);
	real_max = atof(argv[4]);
	imag_min = atof(argv[5]);
	imag_max = atof(argv[6]);

	unsigned char data[disp_height][disp_width];

	/* Load the required colour map file */
	f = fopen(argv[7],"r");
	for(i=0;i<=256;i++) {
		fgets(str,1000,f);
		sscanf(str,"%d %d %d",&(map[0][i]),&(map[1][i]),&(map[2][i]));  
	}
	fclose(f);

	/* Compute scaling factors */
	scale_real = (real_max-real_min)/disp_width;
	scale_imag = (imag_max-imag_min)/disp_height;

	for (y=0; y<disp_height; y++) {
		for (x=0; x<disp_width; x++) {
			c.real = real_min + ((float) x * scale_real);
			c.imag = imag_min + ((float) y * scale_imag);
			i = cal_pixel(c);
			data[y][x] = i;
		}
	}

	f = fopen(argv[8],"w");
	fprintf(f, "P6\n%d %d\n255\n", disp_width, disp_height);
	for (y=0; y<disp_height; y++) {
		for (x=0; x<disp_width; x++) {
			static unsigned char color[3];
			color[0] = map[0][data[y][x]]; // red
			color[1] = map[1][data[y][x]]; // green
			color[2] = map[2][data[y][x]]; // blue
			(void)fwrite(color, 1, 3, f);
		}
	}
	fclose(f);

	gettimeofday(&t2, NULL);
	printTime(0, t1, t2);
}
