/***********************************************************************************/
//	CS789 - Parallel Programming
//	Assignment #4 - System of Equations (Sequential)
//	Sachintha Gurudeniya
//	11/4/2015
/***********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdarg.h>
#include <sys/time.h>

void getSecsUsecs(long long l, long *secs, long *usecs) {
	*secs = l/1000000;
	*usecs = l%1000000;
}

void printTime(int rank, struct timeval t1, struct timeval t2) {
	long long l;
	long secs, usecs;

	l = t2.tv_sec*1000000+t2.tv_usec-(t1.tv_sec*1000000+t1.tv_usec);

	secs = l/1000000;
	usecs = l%1000000;

	printf("Rank = %d: (Start = %ld.%d -> End = %ld.%d (Time = %ld.%ld)\n", rank, t1.tv_sec, t1.tv_usec, t2.tv_sec, t2.tv_usec, secs,usecs);
}

long long getTime(int rank, struct timeval t1, struct timeval t2) {
	long long l;

	l = t2.tv_sec*1000000+t2.tv_usec-(t1.tv_sec*1000000+t1.tv_usec);

	return l;
}

int main(int argc, char *argv[]) {
	/*------ Time Start ------*/
	long totalU = 0;
	long totalS = 0;
	long long totalTime = 0;
	struct timeval t1, t2;

	gettimeofday(&t1, NULL);
	/*------------------------*/

	int n, sum;
	long double *x, *b;
	long double** a;
	int i, j;
	FILE *fa, *fb;
	char str[256];
	int max, ret;
	
	if (argc != 4) {
		printf("Usage: Equation <n> <fa> <fb>\n");
		printf("\t n \t = Number of iterations.\n");
		printf("\t fa \t = File containing a variables.\n");
		printf("\t b0 \t = File containing b variables.)\n");
		printf("%d args supplied\n", argc);
		exit(0);
	}

	/* Get variables from commando line */
	n   = atoi(argv[1]);	// Number of iterations

	x = (long double *)calloc(n, sizeof(long double));
	b = (long double *)calloc(n, sizeof(long double));
	a = (long double**)malloc(sizeof(long double*) * n);
	for (i = 0; i < n; i++) {
		a[i] = (long double*)malloc(sizeof(long double) * n);
	}

	fa = fopen(argv[2], "r");
	max = 1;
	for(i = 0; i < n; i++)
	{
		for(j = 0; j < max; j++)
		{
			ret = fscanf(fa, "%Lf", &a[i][j]);
		}
		max++;
	}
	fclose(fa);

	fb = fopen(argv[3], "r");
	for(i = 0; i < n; i++) {
		ret = fgets(str, 1000, fb);
		sscanf(str, "%Lf", &b[i]);
	}
	fclose(fb);

	x[0] = b[0] / a[0][0];
	for (i = 0; i < n; i++) {
		sum = 0;
		for (j = 0; j < i; j++) {
			sum = sum + a[i][j] * x[j];
		}
		x[i] = (b[i] - sum) / a[i][i];
	}

	for (i = 0; i < n; i++) {
		printf("x[%d] = %Lf ", i, x[i]);
		printf("\n");
	}

	free(x);
	free(a);
	free(b);

	gettimeofday(&t2, NULL);
	totalTime = getTime(0, t1, t2);
	getSecsUsecs(totalTime, &totalS, &totalU);
	printf("Rank = %d, TIME = %ld.%ld\n", 0, totalS, totalU);

	return 0;
}
