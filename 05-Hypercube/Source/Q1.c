/***********************************************************************************/
//	CS789 - Parallel Programming
//	Sachintha Gurudeniya
//	11/28/2015
//	Assignment #6 - Hypercube Routing (Parallel)
//	Version : Q2
/***********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "mpi.h"

#define	SOURCE	0
#define	DEST	1
#define	MSGNO	2
#define	TYPE	3
#define	HOPCNT	4
#define	MSGSIZE	25

#define TAG			11111

// Message Types
#define	MSG		0

const unsigned int power2[12] = {1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048};
int MASTER;
int MsgCounter = 0;

char *getMsgType(int type) 
{
	char* str = (char*)malloc(sizeof(char*) * 8);
	switch (type) {
		case MSG:
			strcpy(str, "MSG");
			break;
	}
	return str;
}

unsigned int compute_next_dest(unsigned int rank, unsigned int dest)
{
	if (rank == dest)
	{
		return dest;
	}

	unsigned int m = rank ^ dest;
	int c = 0;

	// Find the first "1" searching the string from right to left
	while(m % 2 == 0) {	// While m is even
		c++;
		m >>= 1;
	}

	return power2[c] ^ rank;
}

char *byte2bin(unsigned int x, int dim)
{
	int z;
	char* b = (char*)malloc(sizeof(char*) * (dim));

	b[0] = '\0';
	for (z = power2[dim-1]; z > 0; z >>= 1) {
		strcat(b, ((x & z) == z) ? "1" : "0");
	}

	return b;
}

int RandomNumber(int max)
{
	int i, num;
	time_t t;

	/* Intializes random number generator */
	struct timeval tm;
	gettimeofday(&tm, NULL);
	srandom(tm.tv_sec + tm.tv_usec * 1000000ul);

	num = random() % max;

	return num;
}

void CreateMessage(int *msg, int source, int dest, int type, int msgno)
{
	msg[SOURCE] = source;
	msg[DEST] = dest;
	msg[TYPE] = type;
	msg[HOPCNT] = 0;

	if (msgno > 0) {
		msg[MSGNO] = msgno;
	}
	else {
		msg[MSGNO] = source * 100 + MsgCounter;
		MsgCounter++;
	}
}

void PrintMessage(int *msg, int rank, int dim)
{
	int i;
	int hops = 0;
	char path[256];

	if (msg[SOURCE] != msg[DEST]) {
		hops = 1;
	}

	//	Printing SOURCE
	strcpy(path, byte2bin(msg[SOURCE], dim));
	
	//	Printing intermediate nodes
	if (msg[HOPCNT] > 0) {
		for (i = 0; i < msg[HOPCNT]; i++) {
			if (msg[HOPCNT + i + 1] != msg[DEST])
			{
				strcat(path, "->");
				strcat(path, byte2bin(msg[HOPCNT + i + 1], dim));
				hops++;
			}
		}
	}
	
	//	Printing DEST
	strcat(path, "->");
	strcat(path, byte2bin(msg[DEST], dim));

	printf("%d(%s):\t\tMessage (%s) #%04d\t from %d(%s) to %d(%s) in %d hops (%s)\n", 
		rank, 
		byte2bin(rank, dim), 
		getMsgType(msg[TYPE]), 
		msg[MSGNO], 
		msg[SOURCE], 
		byte2bin(msg[SOURCE], dim), 
		msg[DEST], 
		byte2bin(msg[DEST], dim), 
		hops, 
		path);
}

int main(int argc, char *argv[])
{
	MPI_Status status;
	MPI_Comm comm;
	int flag;
	int MPI_ERR;
	int rank, size;
	int dim, numMsgs;
	int* Msg;
	int i, rnd, sentCount, next;

	if (argc != 3) {
		printf("Usage: Hypercube <Dim> <Num of Msgs>\n");
		printf("\t Dim \t = Number of Dimensions\n");
		printf("\t Num of Msgs \t = Number of Messages to be sent\n");
		printf("%d args supplied\n", argc);
		exit(0);
	}

	MPI_ERR = MPI_Init(&argc, &argv);
	MPI_ERR = MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_ERR = MPI_Comm_size(MPI_COMM_WORLD, &size);

	MASTER	= size - 1;
	dim		= atoi(argv[1]);
	numMsgs	= atoi(argv[2]);

	Msg = (int *)calloc(MSGSIZE, sizeof(int));

	if (rank == MASTER) {
		//	MASTER CODE
	}
	else {
		//	SLAVE CODE
		sentCount = 0;

		while(1) {
			MPI_ERR = MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &status);
			if (flag == 1) {
				MPI_ERR = MPI_Recv(&Msg[0], MSGSIZE, MPI_INT, status.MPI_SOURCE, TAG, MPI_COMM_WORLD, &status);

				if (Msg[DEST] == rank) {
					PrintMessage(Msg, rank, dim);
				}
				else {
					Msg[HOPCNT] = Msg[HOPCNT] + 1;
					Msg[HOPCNT + Msg[HOPCNT]] = rank;
					next = compute_next_dest(rank, Msg[DEST]);
					MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);
				}
			}
			else {
				if (sentCount < numMsgs) {
					rnd = RandomNumber(size-1);
					CreateMessage(Msg, rank, rnd, MSG, 0);
					next = compute_next_dest(rank, Msg[DEST]);
					MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);
					sentCount++;
				}
			}
		}
	}

	MPI_ERR = MPI_Finalize();
	return 0;
}
