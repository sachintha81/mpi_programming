/***********************************************************************************/
//	CS789 - Parallel Programming
//	Assignment #5 - Matrix Multiplication (Parallel)
//	Sachintha Gurudeniya
//	11/23/2015
/***********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdarg.h>
#include "mpi.h"
#include <sys/time.h>

#define TAG_BROADCAST		99999
#define TAG_CONVERGECAST	11111
#define	TAG_PIPE			88888
#define	TAG_ROLL			77777

MPI_Status status;
int MPI_ERR;
int MASTER;
int M, N;
int numBlocks, blockSize, subMatrixSize;
int timeRank;
//int debug_rank;

void getSecsUsecs(long long l, long *secs, long *usecs) {
	*secs = l/1000000;
	*usecs = l%1000000;
}

void printTime(int rank, struct timeval t1, struct timeval t2) {
	long long l;
	long secs, usecs;

	l = t2.tv_sec*1000000+t2.tv_usec-(t1.tv_sec*1000000+t1.tv_usec);

	secs = l/1000000;
	usecs = l%1000000;

	printf("Rank = %d: (Start = %ld.%ld -> End = %ld.%ld (Time = %ld.%ld)\n", rank, t1.tv_sec, t1.tv_usec, t2.tv_sec, t2.tv_usec, secs,usecs);
}

long long getTime(int rank, struct timeval t1, struct timeval t2) {
	long long l;

	l = t2.tv_sec*1000000+t2.tv_usec-(t1.tv_sec*1000000+t1.tv_usec);

	return l;
}

void CreateMatrix(char *fileName, int size) {
	int i, j;
	FILE *f = fopen(fileName, "w");

	srand(15);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			fprintf(f, "%d\t", rand() % 3);
		}
		fprintf(f, "\n");
	}
	
	fclose(f);
}

void WriteFileResult(char *fileName, int* C) /* Write result matrix C */
{
	FILE *f;
	int i, j;
	f = fopen(fileName, "w");
	for (i = 0; i < M; i++) {
		for (j = 0; j < M; j++) {
			fprintf(f, "%d\t", C[(i * M) + j]);
		}
		fprintf(f, "\n");
	}
	fclose(f);
}

void PrintMatrix(int* MATRIX, int size) {
	int i, j;
	for(i = 0; i < size; i++) {
		for(j = 0; j < size; j++) {
			printf("%d\t", MATRIX[(size * i) + j]);
		}
		printf("\n");
	}
}

void PrintArray(int* MATRIX, int size) {
	int i, j, side;
	side = sqrt(size);
	for(i = 0; i < side; i++) {
		for(j = 0; j < side; j++) {
			printf("%d\t", MATRIX[(i * side) + j]);
		}
		printf("\n");
	}
	printf("\n");
}

int computeSource(int roundNo, int rank) {
	int source;
	int level, j; 

	level = rank / numBlocks;
	j = (level + roundNo) % numBlocks;
	source = (level * numBlocks) + j;

	return source;
}

int computeLast(int rank, int source) {
	int last;
	int level = rank / numBlocks;
	last = (source + numBlocks - 1) % numBlocks;
	last += level * numBlocks;
	return last;
}

int computeNext(int rank) {
	int next;
	next = rank + 1;
	if (next % numBlocks == 0) {
		next = rank - numBlocks + 1;
	}
	return next;
}

int computePrev(int rank) {
	int prev;
	if (rank % numBlocks == 0) {
		prev = rank + numBlocks - 1;
	}
	else {
		prev = rank - 1;
	}
	return prev;
}

int computeUp(int rank) {
	int up;
	up = rank - numBlocks;
	if (up < 0) {
		up = rank + (numBlocks * (numBlocks - 1));
	}
	return up;
}

int computeDown(int rank) {
	int down;
	down = rank + numBlocks;
	if (down >= N) {
		down = rank - (numBlocks * (numBlocks - 1));
	}
	return down;
}

void Pipe_A(int roundNo, int rank, int* A, int* T) {
	int i, source, last, sendTo, recvFrom;

	source = computeSource(roundNo, rank);
	last = computeLast(rank, source);
	sendTo = computeNext(rank);
	recvFrom = computePrev(rank);

	if (rank == source) {
		for (i = 0; i < subMatrixSize; i++) {
			T[i] = A[i];
		}
		MPI_ERR = MPI_Send(&T[0], subMatrixSize, MPI_INT, sendTo, TAG_PIPE, MPI_COMM_WORLD);
	}
	else if (rank != last) {
		MPI_ERR = MPI_Recv(&T[0], subMatrixSize, MPI_INT, recvFrom, TAG_PIPE, MPI_COMM_WORLD, &status);
		MPI_ERR = MPI_Send(&T[0], subMatrixSize, MPI_INT, sendTo, TAG_PIPE, MPI_COMM_WORLD);
	}
	else {
		MPI_ERR = MPI_Recv(&T[0], subMatrixSize, MPI_INT, recvFrom, TAG_PIPE, MPI_COMM_WORLD, &status);
	}
}

void Roll_B(int roundNo, int rank, int* B) {
	int up, down;

	up = computeUp(rank);
	down = computeDown(rank);
	MPI_ERR = MPI_Send(&B[0], blockSize * blockSize, MPI_INT, up, TAG_ROLL, MPI_COMM_WORLD);
	MPI_ERR = MPI_Recv(&B[0], blockSize * blockSize, MPI_INT, down, TAG_ROLL, MPI_COMM_WORLD, &status);
}

void MultiplyMatrix(int* A, int* B, int* C, int size) {
	int i, j, n;
	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			for (n = 0; n < size; n++) {
				C[(i * size) + j] = C[(i * size) + j] + A[(i * size) + n] * B[(n * size) + j];
			}
		}
	}
}

int main(int argc, char *argv[]) {
	/*------ Time Start ------*/
	long totalU = 0;
	long totalS = 0;
	long long totalTime = 0;
	struct timeval t1, t2;

	gettimeofday(&t1, NULL);
	/*------------------------*/
	int rank, size;

	int* A;
	int* B;
	int* C;
	int* T;
	int* TEMP;
	int* SA;
	int* SB;
	int i, j, k, l, n, count, ret;
	int targetSlave, startIndex, level;
	FILE *fA, *fB;

	if (argc != 3) {
		printf("Usage: Matrix <M> <Rank>\n");
		printf("\t M \t = Matrix Size.\n");
		printf("\t Rank \t = Rank to print time.\n");
		printf("%d args supplied\n", argc);
		exit(0);
	}
	// Time Print Guide
	// ----------------------------------------------
	// If rankToPring < 0
	// 		print none
	// If rankToPrint > # nodes
	//		print all
	// If rankToPrint > 0 AND rankToPrint <= # nodes
	//		print only rankToPrint
	// ----------------------------------------------

	MPI_ERR = MPI_Init(&argc, &argv);
	MPI_ERR = MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_ERR = MPI_Comm_size(MPI_COMM_WORLD, &size);

	/* Get variables from commando line */
	//debug_rank		= atoi(argv[2]);
	M				= atoi(argv[1]);
	N				= size - 1;
	MASTER			= size - 1;
	numBlocks		= sqrt(N);
	blockSize 		= M / numBlocks;
	subMatrixSize	= (M * M) / N;
	timeRank		= atoi(argv[2]);

	if (rank == MASTER)
	{
		// MASTER CODE
		CreateMatrix("A.txt", M);
		CreateMatrix("B.txt", M);

		A = (int *)calloc(M * M, sizeof(int));
		B = (int *)calloc(M * M, sizeof(int));
		C = (int *)calloc(M * M, sizeof(int));
		SA = (int *)calloc(subMatrixSize, sizeof(int));
		SB = (int *)calloc(subMatrixSize, sizeof(int));
		TEMP = (int *)calloc(subMatrixSize, sizeof(int));

		fA = fopen("A.txt", "r");
		for(i = 0; i < M; i++) {
			for(j = 0; j < M; j++) {
				ret = fscanf(fA, "%d", &A[(M*i)+j]);
			}
		}
		fclose(fA);

		fB = fopen("B.txt", "r");
		for(i = 0; i < M; i++) {
			for(j = 0; j < M; j++) {
				ret = fscanf(fB, "%d", &B[(M*i)+j]);
			}
		}
		fclose(fB);

		printf("----------- MATRIX A -----------\n");
		PrintArray(A, M * M);
		printf("----------- MATRIX B -----------\n");
		PrintArray(B, M * M);
		printf("\n");

		for (i = 0; i < numBlocks; i++) {
			for (j = 0; j < numBlocks; j++) {
				startIndex = (i * blockSize * M) + (blockSize * j);
				for (k = 0; k < blockSize; k++) {
					for (l = 0; l < blockSize; l++) {
						SA[(k * blockSize) + l] = A[startIndex + ((k * M) + l)];
						SB[(k * blockSize) + l] = B[startIndex + ((k * M) + l)];
					}
				}
				targetSlave = (i * numBlocks) + j;
				MPI_ERR = MPI_Send(&SA[0], subMatrixSize, MPI_INT, targetSlave, TAG_BROADCAST, MPI_COMM_WORLD);
				MPI_ERR = MPI_Send(&SB[0], subMatrixSize, MPI_INT, targetSlave, TAG_BROADCAST, MPI_COMM_WORLD);
			}
		}

		for (n = 0; n < N; n++) {
			MPI_ERR = MPI_Recv(&TEMP[0], subMatrixSize, MPI_INT, n, TAG_CONVERGECAST, MPI_COMM_WORLD, &status);
			level = n / numBlocks;
			startIndex = (level * blockSize * M) + ((n % numBlocks) * blockSize);
			for (i = 0; i < blockSize; i++) {	
				for (j = 0; j < blockSize; j++) {
					C[startIndex] = TEMP[(i * blockSize) + j];
					startIndex++;
				}
				startIndex += blockSize * (numBlocks - 1);
			}
		}

		printf("----------- MATRIX C -----------\n");
		PrintArray(C, M * M);
		printf("\n");
		//WriteFileResult("Output.txt", C);

		free(A);
		free(B);
		free(C);
		free(SA);
		free(SB);
		free(TEMP);
	}
	else
	{	
		// SLAVE CODE
		A = (int *)calloc(subMatrixSize, sizeof(int));
		B = (int *)calloc(subMatrixSize, sizeof(int));
		C = (int *)calloc(subMatrixSize, sizeof(int));
		T = (int *)calloc(subMatrixSize, sizeof(int));

		MPI_ERR = MPI_Recv(&A[0], subMatrixSize, MPI_INT, MASTER, TAG_BROADCAST, MPI_COMM_WORLD, &status);
		MPI_ERR = MPI_Recv(&B[0], subMatrixSize, MPI_INT, MASTER, TAG_BROADCAST, MPI_COMM_WORLD, &status);

		for (i = 0; i < numBlocks; i++) {
			Pipe_A(i, rank, A, T);
			MultiplyMatrix(T, B, C, blockSize);
			Roll_B(i, rank, B);
		}
		MPI_ERR = MPI_Send(&C[0], subMatrixSize, MPI_INT, MASTER, TAG_CONVERGECAST, MPI_COMM_WORLD);

		free(A);
		free(B);
		free(C);
		free(T);
	}

	gettimeofday(&t2, NULL);
	totalTime = getTime(rank, t1, t2);
	getSecsUsecs(totalTime, &totalS, &totalU);

	if (timeRank == rank) {
		printf("Rank = %d, TIME = %ld.%ld\n", rank, totalS, totalU);
	}
	else if (timeRank >= size) {
		printf("Rank = %d, TIME = %ld.%ld\n", rank, totalS, totalU);

	}

	MPI_ERR = MPI_Finalize();

	return 0;
}
