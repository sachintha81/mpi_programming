/***********************************************************************************/
//	CS789 - Parallel Programming
//	Sachintha Gurudeniya
//	11/25/2015
//	Assignment #6 - Hypercube Routing (Parallel)
//	Version : Q1
/***********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

int generateRandom(int max)
{
	int i, num;
	time_t t;

	/* Intializes random number generator */
	struct timeval tm;
	gettimeofday(&tm, NULL);
	srandom(tm.tv_sec + tm.tv_usec * 1000000ul);

	num = random() % max;

	return num;
}

void PrintMessage(int msgno)
{
	printf("---------- MESSAGE -----------\n"
			"Message No: %d\n"
			, msgno);
}

int main(int argc, char *argv[]) {
	int i;
	int num = atoi(argv[1]);
	for (i = 0; i < num; i++) {
		int rnd = generateRandom(5);
		printf("%d\n", rnd);
	}
	PrintMessage(3);
	return 0;
}
