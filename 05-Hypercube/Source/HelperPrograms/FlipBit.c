/***********************************************************************************/
//	Flipping Bits
//	Example
/***********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdarg.h>
#include <sys/time.h>

void printBits(int num) {
	int a = (num >> 0) & 1;
	int b = (num >> 1) & 1;
	int c = (num >> 2) & 1;
	printf("%d%d%d (%d)\n", c, b, a, num);
}

void ChangeBits(int num, int dim) {
	int i;
	int newNum;
	for (i = 0; i < dim; i++) {
		newNum = num ^ 1 << i;
		printBits(newNum);
	}
}

int FlipBits(int num, int bitPos) {
	return num ^ 1 << bitPos;
}

int main(int argc, char *argv[]) {
	int i;
	int num = atoi(argv[1]);
	int dim = atoi(argv[2]);
	printBits(num);
	for (i = 0; i < dim; i++)
	{
		printBits(FlipBits(num, i));
	}
	return 0;
}
