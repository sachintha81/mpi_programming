/***********************************************************************************/
//	CS789 - Parallel Programming
//	Assignment #4 - System of Equations (Sequential)
//	Sachintha Gurudeniya
//	11/4/2015
/***********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdarg.h>

int main(int argc, char *argv[]) {
	int n, sum;
	long double *x, *b;
	long double** a;
	int i, j;
	FILE *fa, *fb;
	char str[256];
	
	if (argc != 4) {
		printf("Usage: Equation <n> <fa> <fb>\n");
		printf("\t n \t = Number of iterations.\n");
		printf("\t fa \t = File containing a variables.\n");
		printf("\t b0 \t = File containing b variables.)\n");
		printf("%d args supplied\n", argc);
		exit(0);
	}

	/* Get variables from commando line */
	n   = atoi(argv[1]);	// Number of iterations

	x = (long double *)calloc(n, sizeof(long double));
	b = (long double *)calloc(n, sizeof(long double));
	a = (long double**)malloc(sizeof(long double*) * n);
	for (i = 0; i < n; i++) {
		a[i] = (long double*)malloc(sizeof(long double) * n);
	}

	fa = fopen(argv[2], "r");
	int max = 1;
	int count, ret;
	for(i = 0; i < n; i++)
	{
	    for(count = 0; count < max; count++)
	    {
	        ret = fscanf(fa, "%Lf", &a[i][count]);
	    }
	    max++;
	}
	fclose(fa);

	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
		{
			printf("a[%d][%d] = %Lf\n", i, j, a[i][j]);
		}
	}
	/*a[0][0] = 7;
	a[1][0] = 3;
	a[1][1] = 4;
	a[2][0] = 5;
	a[2][1] = 2;
	a[2][2] = 3;
*/
	b[0] = 14;
	b[1] = 18;
	b[2] = 28;

	x[0] = b[0] / a[0][0];
	for (i = 0; i < n; i++) {
		sum = 0;
		for (j = 0; j < i; j++) {
			sum = sum + a[i][j] * x[j];
		}
		x[i] = (b[i] - sum) / a[i][i];
	}

	for (i = 0; i < n; i++) {
		printf("x[%d] = %Lf ", i, x[i]);
		printf("\n");
	}

	return 0;
}
