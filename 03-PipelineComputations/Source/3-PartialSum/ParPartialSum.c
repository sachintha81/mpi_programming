/***********************************************************************************/
//	CS789 - Parallel Programming
//	Assignment #4 - Partial Sum
//	Sachintha Gurudeniya
//	11/4/2015
/***********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdarg.h>
#include <mpi.h>

#define MASTER	0
#define	ROOT	1
#define	TAG_BROADCAST	99999
#define	TAG_DATA		88888
#define	TAG_FINAL		77777

double log2(double num) {
	return log(num) / log(2);
}

int main(int argc, char *argv[]) {
	int rank, size;
	int MPI_ERR;
	MPI_Status status;

	int i, j;
	int numEle, finalNumEle;
	int curRank, LHS, RHS, parent;
	int dest, source;

	int *A;
	int N;
	int sum;
	int element;
	int finalSum;
	int limit;

	if (argc != 1) {
		printf("Usage: PartialSum <N>\n");
		//printf("\t N \t = Number of items.\n");
		printf("%d args supplied\n", argc);
		exit(0);
	}

	/* Get variables from commando line */
	//N = atoi(argv[1]);	// Number of items.

	MPI_ERR = MPI_Init(&argc, &argv);
	MPI_ERR = MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_ERR = MPI_Comm_size(MPI_COMM_WORLD, &size);

	N = size-1;

	if (rank == MASTER)
	{
		// Initializing: O(N)
		A = (int*)calloc(N, sizeof(int));
		for (i=1; i <= N; i++)
		{
			A[i-1] = i;
		}
		MPI_ERR = MPI_Send(&A[0], N, MPI_INT, ROOT, TAG_BROADCAST, MPI_COMM_WORLD);

		MPI_ERR = MPI_Recv(&finalSum, 1, MPI_INT, size-1, TAG_FINAL, MPI_COMM_WORLD, &status);
		MPI_ERR = MPI_Send(&finalSum, 1, MPI_INT, ROOT, TAG_FINAL, MPI_COMM_WORLD);
	}
	else
	{
		int flag = 0;
		numEle = N;
		curRank = 1;
		for (i = 0; i < log(N+1); i++)
		{
			if (rank == ROOT)
			{
				finalNumEle = numEle;
				parent = MASTER;
				LHS = curRank + 1;
				RHS = curRank + numEle/2 + 1;
				break;
			}
			numEle = numEle/2;
			LHS = curRank + 1;
			RHS = curRank + numEle + 1;
			parent = curRank;

			if (rank < RHS)
			{
				curRank = LHS;
			}
			else {
				curRank = RHS;
			}
			if (rank == curRank)
			{
				finalNumEle = numEle;
				numEle = numEle/2;
				LHS = curRank + 1;
				RHS = curRank + numEle + 1;
				break;
			}
		}

		A = (int*)calloc(finalNumEle, sizeof(int));
		
		// Receiving the initial broadcast
		MPI_ERR = MPI_Recv(&A[0], finalNumEle, MPI_INT, parent, TAG_BROADCAST, MPI_COMM_WORLD, &status);
		element = A[0];

		// Passing on the array to children
		if (LHS != RHS)
		{
			// Sending to LHS child
			MPI_ERR = MPI_Send(&A[1], (finalNumEle-1)/2, MPI_INT, LHS, TAG_BROADCAST, MPI_COMM_WORLD);
			// Sending to RHS child
			MPI_ERR = MPI_Send(&A[(finalNumEle/2) + 1], (finalNumEle-1)/2, MPI_INT, RHS, TAG_BROADCAST, MPI_COMM_WORLD);
		}		

		// Partial Sum
		sum = element;
		if (rank < N)
		{
			MPI_ERR = MPI_Send(&element, 1, MPI_INT, rank + 1, TAG_DATA, MPI_COMM_WORLD);
		}

		// Sending, receiving and calculating partial sums
		limit = log2(N);
		for (j = 0; j <= limit; j++)
		{
			source = rank - pow(2, j);
			dest = rank + pow(2, j+1);

			if (rank > pow(2, j)) {
				MPI_ERR = MPI_Recv(&element, 1, MPI_INT, source, TAG_DATA, MPI_COMM_WORLD, &status);
				sum += element;
			}
			
			if (dest <= N) {
				MPI_ERR = MPI_Send(&sum, 1, MPI_INT, dest, TAG_DATA, MPI_COMM_WORLD);
			}
		}

		if (rank == N) {
			MPI_ERR = MPI_Send(&sum, 1, MPI_INT, MASTER, TAG_FINAL, MPI_COMM_WORLD);
		}
		// Receiving the initial broadcast
		MPI_ERR = MPI_Recv(&finalSum, 1, MPI_INT, parent, TAG_FINAL, MPI_COMM_WORLD, &status);
		
		// Passing on the array to children
		if (LHS != RHS)
		{
			// Sending to LHS child
			MPI_ERR = MPI_Send(&finalSum, 1, MPI_INT, LHS, TAG_FINAL, MPI_COMM_WORLD);
			// Sending to RHS child
			MPI_ERR = MPI_Send(&finalSum, 1, MPI_INT, RHS, TAG_FINAL, MPI_COMM_WORLD);
		}

		printf("Rank = %d, Partial Sum = %d, Final Sum = %d\n", rank, sum, finalSum);
	}

	MPI_ERR = MPI_Finalize();
	return 0;
}
