# README #

### What is this repository for? ###

* As a part of my MS in Computer Science program at the University of Nevada, Las Vegas, I took the Parallel Programming course. We had five assignments, each of which is included here as a separate folder.

### The five assignments are briefly described below. ###

1. Mandelbrot Computation
Creating a Mandelbrot set image.

2. Vibrating String
Simulate the vibration of a string (the sin-wave).

3. Pipeline Computation
Perform a complex computation using a pipeline to increase the efficiency.

4. Matrix Multiplication
Perform matrix multiplication in very large matrices such as 100x100.

5. Hyper-cube Message Communication
Perform message communication among nodes in a hyper-cube network of n-dimensions.