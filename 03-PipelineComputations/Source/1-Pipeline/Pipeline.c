/***********************************************************************************/
//	CS789 - Parallel Programming
//	Assignment #4 - Pipeline
//	Sachintha Gurudeniya
//	10/28/2015
/***********************************************************************************/

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdarg.h>

#define	TAG_DATA	0

int rank;
long val;
struct timeval t0, t1, t2, t3;
MPI_Status status;
MPI_Request req;
int MPI_ERR;

void proc(int delay, int from, int to) {
	while (1) {
		MPI_ERR = MPI_Recv(&val, 1, MPI_LONG, from, TAG_DATA, MPI_COMM_WORLD, &status);
		//printf("proc MPI_Recv = %d\n", MPI_ERR);
		usleep(delay * 10000);
		MPI_ERR = MPI_Ssend(&val, 1, MPI_LONG, to, TAG_DATA, MPI_COMM_WORLD);
		//printf("proc MPI_Ssend = %d\n", MPI_ERR);
	}
}

void disperser(int noProc, int from, int procs[]) {
	int i;
	while(1) {
		for (i = 0; i < noProc; i++) {
			MPI_ERR = MPI_Recv(&val, 1, MPI_LONG, from, TAG_DATA, MPI_COMM_WORLD, &status);
			//printf("disperser MPI_Recv = %d\n", MPI_ERR);
			MPI_ERR = MPI_Ssend(&val, 1, MPI_LONG, procs[i], TAG_DATA, MPI_COMM_WORLD); 
			//printf("disperser MPI_Ssend = %d\n", MPI_ERR);
		}
	}
}

void collector(int noProc, int to, int procs[]) {
	int i;
	while(1) {
		for (i = 0; i < noProc; i++) {
			MPI_ERR = MPI_Recv(&val, 1, MPI_LONG, procs[i], TAG_DATA, MPI_COMM_WORLD, &status);
			//printf("collector MPI_Recv = %d\n", MPI_ERR);
			MPI_ERR = MPI_Ssend(&val, 1, MPI_LONG, to, TAG_DATA, MPI_COMM_WORLD);
			//printf("collector MPI_Ssend = %d\n", MPI_ERR);
		}
	}
}

void in(int num, int to) {
	int i;
	for(i = 0; i < num; i++) {
		gettimeofday(&t1, NULL);
		val = (t1.tv_sec % 1000) * 1000000 + t1.tv_usec;   
		MPI_ERR = MPI_Ssend(&val, 1, MPI_LONG, to, TAG_DATA, MPI_COMM_WORLD);
		//printf("in MPI_Ssend = %d\n", MPI_ERR);
	}
}

void out(int no, int from) {
 	int i;
 	long long elapsedsum =0;
 	long elapsed;
 	long long transportsum = 0;
	long transport;
	int getlatency = 1;
	long latency;
	gettimeofday(&t0,NULL);
	gettimeofday(&t3,NULL);

	for(i = 0; i < no; i++) {
		MPI_ERR = MPI_Recv(&val, 1, MPI_LONG, from, TAG_DATA, MPI_COMM_WORLD, &status);
		//printf("out MPI_Recv = %d\n", MPI_ERR);

		gettimeofday(&t2, NULL);

		elapsed = (t2.tv_sec % 1000) * 1000000 + t2.tv_usec - (t3.tv_sec % 1000) * 1000000 - t3.tv_usec;
		if (getlatency) {
			latency = elapsed;
			getlatency = 0;
		}
		else {
			elapsedsum += elapsed;
		}
		transport = (t2.tv_sec%1000)*1000000+t2.tv_usec-val;
		transportsum += transport;
		printf("Packet %d - Transport Time ..........: %ld   ", i, transport);
		printf("Elapsed since last packet: %ld\n", elapsed);   
		gettimeofday(&t3,NULL);
	}
	gettimeofday(&t2,NULL);

	printf("Latency...............: %ld\n", latency);
	printf("Total Time............: %ld\n", (t2.tv_sec % 1000) * 1000000 + t2.tv_usec - (t0.tv_sec % 1000) * 1000000 - t0.tv_usec);

	printf("Average Transport Time: %ld\n", (long)(transportsum/no));
	printf("Average Rate..........: %ld\n", (long)(elapsedsum/(no-1)));

	printf("Press enter to terminate\n");
	getchar();
}

int main(int argc, char *argv[]) {
	/* ****************** MPI Variables ****************** */
	int rank, size;
	/* ****************** End of MPI Variables ****************** */

	/* ****************** MPI Init ****************** */
	MPI_ERR = MPI_Init(&argc, &argv);
	MPI_ERR = MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_ERR = MPI_Comm_size(MPI_COMM_WORLD, &size);
	/* ****************** End of MPI Init ****************** */
	
	int a[4];
	int num = atoi(argv[1]);

	switch(rank) {
		case 0: in(num, 1); break;
		case 1: proc(2, 0, 2); break;
		case 2: 
			a[0] = 3;
			a[1] = 4;
			a[2] = 5;
			disperser(3, 1, a); 
			free(a);
			break;
		case 3: 
		case 4: 
		case 5: proc(6, 2, 6); break;
		case 6: 
			a[0] = 3;
			a[1] = 4;
			a[2] = 5;
			collector(3, 7, a); 
			break;
		case 7: 
			a[0] = 8;
			a[1] = 9;
			a[2] = 10;
			disperser(3, 6, a); 
			break;
		case 8: 
		case 9: 
		case 10: proc(6, 7, 11); break;
		case 11: 
			a[0] = 8;
			a[1] = 9;
			a[2] = 10;
			collector(3, 12, a); 
			break;
		case 12: 
			a[0] = 13;
			a[1] = 14;
			a[2] = 15;
			a[3] = 16;
			disperser(4, 11, a); 
			break;
		case 13: 
		case 14: 
		case 15: 
		case 16: proc(8, 12, 17); break;
		case 17: 
			a[0] = 13;
			a[1] = 14;
			a[2] = 15;
			a[3] = 16;
			collector(4, 18, a); 
			break;
		case 18: 
			a[0] = 19;
			a[1] = 20;
			disperser(2, 17, a); 
			break;
		case 19: 
		case 20: proc(4, 18, 21); break;
		case 21: 
			a[0] = 19;
			a[1] = 20;
			collector(2, 22, a); 
			break;
		case 22: proc(2, 21, 23); break;
		case 23: out(num, 22); break;		
	}
	
	MPI_Finalize();
	printf("Done: %d\n", rank);
	return 0;
}
