/***********************************************************************************/
//	CS789 - Parallel Programming
//	Assignment #3 - Vibrating String
//	Sachintha Gurudeniya
//	10/13/2015
/***********************************************************************************/

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>

#define	MASTER_ID	0
#define TAG_INIT	10000
#define	TAG_WORK	20000
#define	TAG_DATA	30000
#define TAG_X		99999
#define TAG_Y		88888
#define TAG_YOLD	77777
#define TAG_YNEW	66666

void getSecsUsecs(long long l, long *secs, long *usecs) {
	*secs = l/1000000;
	*usecs = l%1000000;
}

void printTime(int rank, struct timeval t1, struct timeval t2) {
	long long l;
	long secs, usecs;

	l = t2.tv_sec*1000000+t2.tv_usec-(t1.tv_sec*1000000+t1.tv_usec);

	secs = l/1000000;
	usecs = l%1000000;

	printf("Rank = %d: (Start = %ld.%d -> End = %ld.%d (Time = %ld.%ld)\n", rank, t1.tv_sec, t1.tv_usec, t2.tv_sec, t2.tv_usec, secs,usecs);
}

long long getTime(int rank, struct timeval t1, struct timeval t2) {
	long long l;

	l = t2.tv_sec*1000000+t2.tv_usec-(t1.tv_sec*1000000+t1.tv_usec);

	return l;
}

int main(int argc, char *argv[]) {
	struct timeval t1, t2;
	struct timeval tIO1, tIO2;
	long long totalTime = 0.0;
	long long ioTime = 0.0;
	long totalS, totalU;
	long ioS, ioU;

	gettimeofday(&t1, NULL);

	int n;
	int nb;
	long double l;
	long double dt;
	int steps;
	FILE *y_file;
	long double *x, *y, *yold, *ynew;
	long double pi, tau, dx;
	int i,s;

	/* ****************** Parallel Specific Variables ****************** */
	int workerID;
	int nodesPerWorker;
	int numOfNodes, trueNumOfNodes;
	int startPoint;
	int LeftMostWorkerID, RightMostWorkerID;
	int sendrecvIndex;
	long double lengthPerWorker;
	/* ****************** End of Parallel Specific Variables ****************** */

	if (argc != 6) {
		printf("Usage: wave <l> <nb> <n> <steps> <dt>\n");
		printf("\t l \t = Total length of the string (x-axis).\n");
		printf("\t nb \t = Number of half sine waves.\n");
		printf("\t n \t = Number of nodes (number of discrete points on the x-axis between 0 and l.)\n");
		printf("\t steps \t = Number of steps.\n");
		printf("\t dt \t = Size of each step.\n");
		printf("%d args supplied\n", argc);
		exit(0);
	}

	/* Get variables from commando line */
	l     = atof(argv[1]);	// Length of the string (x-axis)
	nb    = atoi(argv[2]);	// Number of half sine waves
	n     = atoi(argv[3]);	// Number of nodes (number of discrete points on the x-axis between 0 and l.)
	steps = atoi(argv[4]);	// Number of steps
	dt    = atof(argv[5]);	// Size of each step

	/* set variables */
	pi = 4.0 * atan(1);
	dx = l / (n-1);
	tau = 2.0 * l * dt / nb / dx;

	/* ****************** MPI Variables ****************** */
	MPI_Status status;
	int MPI_ERR;
	int rank, size;
	/* ****************** End of MPI Variables ****************** */

	/* ****************** MPI Init ****************** */
	MPI_ERR = MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	/* ****************** End of MPI Init ****************** */

	LeftMostWorkerID = 1;
	RightMostWorkerID = size-1;
	nodesPerWorker = n/(size-1);
	lengthPerWorker = l/(long double)(size-1);

	if (rank == MASTER_ID) {
	/* ****************** MASTER ****************** */
		x = (long double *)calloc(n, sizeof(long double));
		y = (long double *)calloc(n, sizeof(long double));
		yold = (long double *)calloc(n, sizeof(long double));
		ynew = (long double *)calloc(n, sizeof(long double));

		/* initialize x-array */
		for (i=0; i<n; i++) {
			x[i] = l*i/(n-1);
		}
		/* initialize y*-arrays */
		for (i=0; i<n; i++) {
			if ((i==0) || (i == n-1))
				y[i] = yold[i] = ynew[i] = 0.0;
			else
				y[i] = yold[i] = sin(pi*nb*x[i]/l);
		}

		int sendAmount;
		for (i = 1; i < size; i++)
		{
			numOfNodes = nodesPerWorker + (n % (size-1) >= i? 1 : 0);
			startPoint = (nodesPerWorker * (i-1)) + (i > n % (size-1) ? n % (size-1) : i-1);

			sendrecvIndex = startPoint - 1;
			sendAmount = numOfNodes + 2;
			if (i == LeftMostWorkerID) {
				sendrecvIndex = 0;
				sendAmount = numOfNodes + 1;
			}
			if (i == RightMostWorkerID) {
				sendAmount = numOfNodes + 1;
			}

			MPI_ERR = MPI_Send(&x[sendrecvIndex], sendAmount, MPI_LONG_DOUBLE, i, TAG_X, MPI_COMM_WORLD);
			MPI_ERR = MPI_Send(&y[sendrecvIndex], sendAmount, MPI_LONG_DOUBLE, i, TAG_Y, MPI_COMM_WORLD);
			MPI_ERR = MPI_Send(&yold[sendrecvIndex], sendAmount, MPI_LONG_DOUBLE, i, TAG_YOLD, MPI_COMM_WORLD);
			MPI_ERR = MPI_Send(&ynew[sendrecvIndex], sendAmount, MPI_LONG_DOUBLE, i, TAG_YNEW, MPI_COMM_WORLD);
		}

		for (i = 1; i < size; i++)
		{
			MPI_ERR = MPI_Recv(&workerID, 1, MPI_INT, MPI_ANY_SOURCE, TAG_INIT, MPI_COMM_WORLD, &status);
			
			numOfNodes = nodesPerWorker + (n % (size-1) >= workerID? 1 : 0);
			startPoint = (nodesPerWorker * (workerID-1)) + (workerID > n % (size-1) ? n % (size-1) : workerID-1);
		
			MPI_ERR = MPI_Recv(&y[startPoint], numOfNodes, MPI_LONG_DOUBLE, workerID, TAG_DATA, MPI_COMM_WORLD, &status);
		}

		gettimeofday(&tIO1, NULL);
		/* Write the result to a file*/
		y_file = fopen("resultPar.txt", "w" );
		if (y_file == (FILE *)NULL) {
			printf("Could not open output file.\n");
			exit(1);
		}

		for (i=0; i<n; i++) {
			fprintf(y_file,"%Lf %15.15Lf\n",( l * i )/( n - 1 ), y[i]);
		}
		fclose(y_file);
		gettimeofday(&tIO2, NULL);
		ioTime = getTime(rank, tIO1, tIO2);
		getSecsUsecs(ioTime, &ioS, &ioU);
		printf("Rank = %d, I/O Time = %ld.%ld\n", rank, ioS, ioU);

		free(x);
		free(y);
		free(yold);
		free(ynew);
	}
	else {
	/* ****************** WORKER ****************** */
		trueNumOfNodes = nodesPerWorker + (n % (size-1) >= rank? 1 : 0);
		startPoint = (nodesPerWorker * (rank-1)) + (rank > n % (size-1) ? n % (size-1) : rank-1);
		
		if(rank == LeftMostWorkerID || rank == RightMostWorkerID)
			numOfNodes = trueNumOfNodes + 1;
		else
			numOfNodes = trueNumOfNodes + 2;

		x = (long double *)calloc(numOfNodes, sizeof(long double));
		y = (long double *)calloc(numOfNodes, sizeof(long double));
		yold = (long double *)calloc(numOfNodes, sizeof(long double));
		ynew = (long double *)calloc(numOfNodes, sizeof(long double));

		if (rank == LeftMostWorkerID)
			sendrecvIndex = 0;
		else
			sendrecvIndex = 1;

		MPI_ERR = MPI_Recv(&x[0], numOfNodes, MPI_LONG_DOUBLE, MASTER_ID, TAG_X, MPI_COMM_WORLD, &status);
		MPI_ERR = MPI_Recv(&y[0], numOfNodes, MPI_LONG_DOUBLE, MASTER_ID, TAG_Y, MPI_COMM_WORLD, &status);
		MPI_ERR = MPI_Recv(&yold[0], numOfNodes, MPI_LONG_DOUBLE, MASTER_ID, TAG_YOLD, MPI_COMM_WORLD, &status);
		MPI_ERR = MPI_Recv(&ynew[0], numOfNodes, MPI_LONG_DOUBLE, MASTER_ID, TAG_YNEW, MPI_COMM_WORLD, &status);

		//printf("WorkerID = %d, numOfNodes = %d, startPoint = %d\n", rank, numOfNodes, startPoint);

		/* Perform calculations */
		for (s=0; s<steps; s++) {
			if (rank > LeftMostWorkerID)	// Send to left
				MPI_ERR = MPI_Send(&y[1], 1, MPI_LONG_DOUBLE, rank-1, TAG_WORK, MPI_COMM_WORLD);
	
			if (rank < RightMostWorkerID)	// Send to right
				MPI_ERR = MPI_Send(&y[numOfNodes-2], 1, MPI_LONG_DOUBLE, rank+1, TAG_WORK, MPI_COMM_WORLD);
	
			if (rank > LeftMostWorkerID)	// Receive from left
				MPI_ERR = MPI_Recv(&y[0], 1, MPI_LONG_DOUBLE, rank-1, TAG_WORK, MPI_COMM_WORLD, &status);
	
			if (rank < RightMostWorkerID)	// Receive from right
				MPI_ERR = MPI_Recv(&y[numOfNodes-1], 1, MPI_LONG_DOUBLE, rank+1, TAG_WORK, MPI_COMM_WORLD, &status);
			
			for (i=1; i<numOfNodes-1; i++) { 
				ynew[i] = 2.0*y[i]-yold[i]+tau*tau*(y[i-1]-2.0*y[i]+y[i+1]);
			}
			for (i=1; i<numOfNodes-1; i++) {
				yold[i] = y[i];
				y[i] = ynew[i];
			}
		}

		MPI_ERR = MPI_Send(&rank, 1, MPI_INT, MASTER_ID, TAG_INIT, MPI_COMM_WORLD);
		MPI_ERR = MPI_Send(&y[sendrecvIndex], trueNumOfNodes, MPI_LONG_DOUBLE, MASTER_ID, TAG_DATA, MPI_COMM_WORLD);

		free(x);
		free(y);
		free(yold);
		free(ynew);		
	}

	MPI_Finalize();

	gettimeofday(&t2, NULL);
	totalTime = getTime(rank, t1, t2);
	getSecsUsecs(totalTime, &totalS, &totalU);
	printf("Rank = %d, Total Time = %ld.%ld\n", rank, totalS, totalU);
	
	return 0;
}
