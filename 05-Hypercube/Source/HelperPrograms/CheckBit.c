/***********************************************************************************/
//	Checking Bits
//	Example
/***********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdarg.h>
#include <sys/time.h>

int CheckBit(int num, int bit)
{
	return (num >> bit) & 1;
}

int main(int argc, char *argv[]) {
	int output, i;
	int num = atoi(argv[1]);
	int bit = atoi(argv[2]);
	
	for (i = num; i >= 0; i--) {
		output = (i >> bit) & 1;
		printf("Num: %d, Bit: %d\n", i, output);
	}
	return 0;
}
