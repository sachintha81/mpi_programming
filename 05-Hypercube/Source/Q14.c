/***********************************************************************************/
//	CS789 - Parallel Programming
//	Sachintha Gurudeniya
//	11/29/2015
//	Assignment #6 - Hypercube Routing (Parallel)
//	Version : Q13 & Q14
/***********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdarg.h>
#include <sys/time.h>

void printBits(int num, int dim) {
	int i;
	for (i = dim-1; i >= 0; i--) {
		printf("%d", (num >> i) & 1);
	} 
	printf("(%d)", num);
}

int parity(int n)
{
	int c = 0;
	while (n > 0) {
		if (n%2 == 1)
			c++;
		n >>= 1;
	}
	return c;
}

int findFirst(int i)
{
	int c = 0;
	while (i % 2 == 0) {
		c++;
		i >>= 1;
	}
	return c;
}

int FlipBits(int num, int bitPos)
{
	return num ^ 1 << bitPos;
}

int myPrev(int num, int dim)
{
	int prev;
	int ones;
	int bitToFlip;

	/* myAddress is 000...000 */
	if (num == 0) {
		prev = pow(2, dim-1);
	}
	else {
		ones = parity(num);

		/* Odd # of 1s */
		if (ones % 2 == 1) {
			/* Flip LSB */
			bitToFlip = 0;
		}
		/* Even # of 1s */
		else {
			/* Flip bit to the left of rightmost 1 */
			bitToFlip = findFirst(num) + 1;
		}
		prev = num ^ 1 << bitToFlip;
	}

	return prev;
}

int myNext(int num, int dim)
{
	int next;
	int ones;
	int bitToFlip;

	/* myAddress is 100...000 */
	if (num == pow(2, dim-1)) {
		next = 0;
	}
	else {
		ones = parity(num);
		
		/* Odd # of 1s */
		if (ones % 2 == 1) {
			/* Flip bit to the left of rightmost 1 */
			bitToFlip = findFirst(num) + 1;
		}
		/* Even # of 1s */
		else {
			/* Flip LSB */
			bitToFlip = 0;
		}
		next = num ^ 1 << bitToFlip;
	}

	return next;
}

int main(int argc, char *argv[]) {
	int dim = atoi(argv[1]);
	int i;
	int num;
	
	num = myNext(0, dim);
	printBits(num, dim);
	do {
		printf(" -> ");
		num = myNext(num, dim);
		printBits(num, dim);
	} while(num != 0);

	printf("\n");
	printf("\n");

	num = myPrev(0, dim);
	printBits(num, dim);
	do {
		printf(" -> ");
		num = myPrev(num, dim);
		printBits(num, dim);
	} while(num != 0);
	printf("\n");

	return 0;
}
