/***********************************************************************************/
//	CS789 - Parallel Programming
//	Assignment #4 - System of Equations (Parallel)
//	Sachintha Gurudeniya
//	11/4/2015
/***********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdarg.h>
#include "mpi.h"
#include <sys/time.h>

#define MASTER	0
#define TAG		10000
#define TAG_RET	20000

void getSecsUsecs(long long l, long *secs, long *usecs) {
	*secs = l/1000000;
	*usecs = l%1000000;
}

void printTime(int rank, struct timeval t1, struct timeval t2) {
	long long l;
	long secs, usecs;

	l = t2.tv_sec*1000000+t2.tv_usec-(t1.tv_sec*1000000+t1.tv_usec);

	secs = l/1000000;
	usecs = l%1000000;

	printf("Rank = %d: (Start = %ld.%d -> End = %ld.%d (Time = %ld.%ld)\n", rank, t1.tv_sec, t1.tv_usec, t2.tv_sec, t2.tv_usec, secs,usecs);
}

long long getTime(int rank, struct timeval t1, struct timeval t2) {
	long long l;

	l = t2.tv_sec*1000000+t2.tv_usec-(t1.tv_sec*1000000+t1.tv_usec);

	return l;
}

int main(int argc, char *argv[]) {
	/*------ Time Start ------*/
	long totalU = 0;
	long totalS = 0;
	long long totalTime = 0;
	struct timeval t1, t2;

	gettimeofday(&t1, NULL);
	/*------------------------*/

	MPI_Status status;

	int rank, size;
	int MPI_ERR;

	int n, sum;
	long double *x, *b, *a;
	int i, j;
	FILE *fa, *fb;
	char str[256];
	int max, ret;
	
	if (argc != 4) {
		printf("Usage: Equation <n> <fa> <fb>\n");
		printf("\t n \t = Number of iterations.\n");
		printf("\t fa \t = File containing a variables.\n");
		printf("\t b0 \t = File containing b variables.)\n");
		printf("%d args supplied\n", argc);
		exit(0);
	}

	MPI_ERR = MPI_Init(&argc, &argv);
	MPI_ERR = MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_ERR = MPI_Comm_size(MPI_COMM_WORLD, &size);

	/* Get variables from commando line */
	n   = atoi(argv[1]);	// Number of iterations

	if (rank == MASTER) {
		x = (long double *)calloc(n, sizeof(long double));
		b = (long double *)calloc(n, sizeof(long double));
		a = (long double *)calloc(n*n, sizeof(long double));

		// File read
		fa = fopen(argv[2], "r");
		max = 1;
		for(i = 0; i < n; i++)
		{
			for(j = 0; j < max; j++)
			{
				ret = fscanf(fa, "%Lf", &a[(n*i)+j]);
			}
			max++;
		}
		fclose(fa);

		fb = fopen(argv[3], "r");
		for(i = 0; i < n; i++) {
			ret = fgets(str, 1000, fb);
			sscanf(str, "%Lf", &b[i]);
		}
		fclose(fb);

		for (i = 1; i < size; i++)
		{
			MPI_ERR = MPI_Send(&a[n * (i-1)], i, MPI_LONG_DOUBLE, i, TAG, MPI_COMM_WORLD);
			MPI_ERR = MPI_Send(&b[i-1], 1, MPI_LONG_DOUBLE, i, TAG, MPI_COMM_WORLD);
		}

		for (i = 1; i < size; i++)
		{
			MPI_ERR = MPI_Recv(&x[i-1], 1, MPI_LONG_DOUBLE, MPI_ANY_SOURCE, TAG_RET, MPI_COMM_WORLD, &status);
		}
		
		for (i = 0; i < n; i++)
		{
			printf("x[%d] = %Lf\n", i, x[i]);
		}

		free(a);
		free(b);
		free(x);
	}
	else {
		a = (long double *)calloc(rank, sizeof(long double));
		b = (long double *)calloc(1, sizeof(long double));
		x = (long double *)calloc(rank, sizeof(long double));

		MPI_ERR = MPI_Recv(&a[0], rank, MPI_LONG_DOUBLE, MASTER, TAG, MPI_COMM_WORLD, &status);
		MPI_ERR = MPI_Recv(&b[0], 1, MPI_LONG_DOUBLE, MASTER, TAG, MPI_COMM_WORLD, &status);
		
		if (rank == 1) {
			x[0] = b[0] / a[0];
			MPI_ERR = MPI_Send(&x[0], 1, MPI_LONG_DOUBLE, rank+1, TAG, MPI_COMM_WORLD);
			MPI_ERR = MPI_Send(&x[0], 1, MPI_LONG_DOUBLE, MASTER, TAG_RET, MPI_COMM_WORLD);
		}
		if (rank != 1) {
			sum = 0;
			for (i = 0; i < rank-1; i++) {
				MPI_ERR = MPI_Recv(&x[i], 1, MPI_LONG_DOUBLE, rank-1, TAG, MPI_COMM_WORLD, &status);
				//printf("Slave = %d, Received %Lf from Slave %d\n", rank, x[i], rank-1);
				if (rank != size-1) {
					MPI_ERR = MPI_Send(&x[i], 1, MPI_LONG_DOUBLE, rank+1, TAG, MPI_COMM_WORLD);
					//printf("Slave = %d, Sent %Lf to Slave %d\n", rank, x[i], rank+1);
				}
				sum += a[i] * x[i];
			}
			x[rank-1] = (b[0] - sum) / a[rank-1];
			if (rank != size-1)
			{
				MPI_ERR = MPI_Send(&x[rank-1], 1, MPI_LONG_DOUBLE, rank+1, TAG, MPI_COMM_WORLD);
			}
			MPI_ERR = MPI_Send(&x[rank-1], 1, MPI_LONG_DOUBLE, MASTER, TAG_RET, MPI_COMM_WORLD);
		}

		free(a);
		free(b);
		free(x);
	}

	gettimeofday(&t2, NULL);
	totalTime = getTime(rank, t1, t2);
	getSecsUsecs(totalTime, &totalS, &totalU);
	printf("Rank = %d, TIME = %ld.%ld\n", rank, totalS, totalU);

	MPI_ERR = MPI_Finalize();
	return 0;
}
