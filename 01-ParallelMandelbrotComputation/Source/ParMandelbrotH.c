/***********************************************************************************/
//	CS789 - Parallel Programming
//	Assignment #2 - Parallel Mandelbrot
//	Sachintha Gurudeniya
//	10/07/2015
//	
//	Implementation:
//	---------------
//	The image is divided horizontally and equally among workers.
//	The master doesn't do computations. Receives chunks of data and writes them
//	all at once to the ppm file.
/***********************************************************************************/

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#define	MASTER_ID	0
#define	TAG_INIT	10000
#define	TAG_DATA	20000

struct complex {
	float real;
	float imag;
};

void getSecsUsecs(long long l, long *secs, long *usecs) {
	*secs = l/1000000;
	*usecs = l%1000000;
}

void printTime(int rank, struct timeval t1, struct timeval t2) {
	long long l;
	long secs, usecs;

	l = t2.tv_sec*1000000+t2.tv_usec-(t1.tv_sec*1000000+t1.tv_usec);

	secs = l/1000000;
	usecs = l%1000000;

	printf("Rank = %d: (Start = %ld.%d -> End = %ld.%d (Time = %ld.%ld)\n", rank, t1.tv_sec, t1.tv_usec, t2.tv_sec, t2.tv_usec, secs,usecs);
}

long long getTime(int rank, struct timeval t1, struct timeval t2) {
	long long l;

	l = t2.tv_sec*1000000+t2.tv_usec-(t1.tv_sec*1000000+t1.tv_usec);

	return l;
}

int cal_pixel(struct complex c) {
	int count, max;
	struct complex z;
	float temp, lengthsq;

	max = 256;
	z.real = 0;
	z.imag = 0;
	count =0;
	
	do {
		float realSq = z.real * z.real;
		float imagSq = z.imag * z.imag;
		z.imag = 2 * z.real * z.imag + c.imag;
		z.real = realSq - imagSq + c.real;
		lengthsq = realSq + imagSq;
		count++;
	} while ((lengthsq < 4.0) && (count < max));
	
	return count;
}

int main(int argc, char *argv[]) {
	/*------ Time Start ------*/
	long ioTotalSecs = 0;
	long ioTotalUSecs = 0;
	long long ioTotal = 0;
	struct timeval iot1, iot2;

	long commTotalSecs = 0;
	long commTotalUSecs = 0;
	long long commTotal = 0;
	struct timeval commt1, commt2;

	long compTotalSecs = 0;
	long compTotalUSecs = 0;
	long long compTotal = 0;
	struct timeval comp1, comp2;

	long grandTotalSecs = 0;
	long grandTotalUSecs = 0;
	long long grandTotal = 0;
	struct timeval t1, t2;

	gettimeofday(&t1, NULL);
	/*------------------------*/

	int disp_width, disp_height;
	float real_min, real_max, imag_min, imag_max, scale_real, scale_imag;
	FILE *f;
	struct complex c;
	int x,y,i;
	char str[256];

	int map[3][257];
	int *pic;

	if (argc != 9) {
		printf("Usage:\n Mandelbrot width height real-min real-max imag-min imag-max mapfile outfile\n");
		exit(1);
	}

	/* Decode arguments */
	disp_width  = atoi(argv[1]);
	disp_height = atoi(argv[2]);

	real_min = atof(argv[3]);
	real_max = atof(argv[4]);
	imag_min = atof(argv[5]);
	imag_max = atof(argv[6]);

	/* ****************** MPI Variables ****************** */
	MPI_Status status;
	int MPI_ERR;
	int rank, size;
	/* ****************** End of MPI Variables ****************** */

	/* ****************** MPI Init ****************** */
	MPI_ERR = MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	/* ****************** End of MPI Init ****************** */

	int startRow, endRow, procHeight;
	int rowsPerWorker = disp_height / (size - 1);
	int workerID, yProc;

	//pic = (int *)malloc(disp_width*disp_height*sizeof(int*));
	pic = (int *)malloc(disp_width*disp_height*sizeof(int));

	if (rank == MASTER_ID) {
	/* ****************** MASTER ****************** */
		/* Load the required colour map file */
		gettimeofday(&iot1, NULL);
		f = fopen(argv[7],"r");
		for(i=0;i<=256;i++) {
			fgets(str,1000,f);
			sscanf(str,"%d %d %d",&(map[0][i]),&(map[1][i]),&(map[2][i]));  
		}
		fclose(f);
		gettimeofday(&iot2, NULL);
		ioTotal += getTime(rank, iot1, iot2);
				
		int i;
		for (i = 1; i < size; i++)
		{
			gettimeofday(&commt1, NULL);
			MPI_ERR = MPI_Recv(&workerID, 1, MPI_INT, MPI_ANY_SOURCE, TAG_INIT, MPI_COMM_WORLD, &status);
			gettimeofday(&commt2, NULL);
			commTotal += getTime(rank, commt1, commt2);

			procHeight = rowsPerWorker + (disp_height%(size-1) >= rank ? 1 : 0);
			startRow = (rowsPerWorker * (workerID - 1)) + (workerID > disp_height%(size-1) ? disp_height%(size-1) : workerID-1);

			gettimeofday(&commt1, NULL);
			MPI_ERR = MPI_Recv(&pic[startRow*disp_width], disp_width*procHeight, MPI_INT, workerID, TAG_DATA, MPI_COMM_WORLD, &status);
			gettimeofday(&commt2, NULL);
			commTotal += getTime(rank, commt1, commt2);

			//printf("WORKER = %d, startRow = %d\n", workerID, startRow);
		}

		gettimeofday(&iot1, NULL);
		f = fopen(argv[8],"w");
		fprintf(f, "P6\n%d %d\n255\n", disp_width, disp_height);
		for (y=0; y<disp_height; y++) {
			for (x=0; x<disp_width; x++) {
				static unsigned char color[3];
				color[0] = map[0][pic[y*disp_width + x]]; // red
				color[1] = map[1][pic[y*disp_width + x]]; // green
				color[2] = map[2][pic[y*disp_width + x]]; // blue
				(void)fwrite(color, 1, 3, f);
			}
		}
		fclose(f);
		gettimeofday(&iot2, NULL);
		ioTotal += getTime(rank, iot1, iot2);
	}
	/* ****************** End of MASTER ****************** */
	else {
	/* ****************** WORKER ****************** */
		gettimeofday(&comp1, NULL);
		procHeight = rowsPerWorker + (disp_height%(size-1) >= rank ? 1 : 0);
		startRow = (rowsPerWorker * (rank - 1)) + (rank > disp_height%(size-1) ? disp_height%(size-1) : rank-1);

		//printf("procHeight = %d, startRow = %d\n", procHeight, startRow);
		pic = (int *)malloc(procHeight*disp_width*sizeof(int));

		/* Compute scaling factors */
		scale_real = (real_max-real_min)/disp_width;
		scale_imag = (imag_max-imag_min)/disp_height;

		for (y=startRow, yProc = 0; y<startRow + procHeight; y++, yProc++) {
			for (x=0; x<disp_width; x++) {
				c.real = real_min + ((float) x * scale_real);
				c.imag = imag_min + ((float) y * scale_imag);
				i = cal_pixel(c);
				pic[yProc*disp_width + x] = i;
			}
		}
		gettimeofday(&comp2, NULL);
		compTotal += getTime(rank, comp1, comp2);

		gettimeofday(&commt1, NULL);
		MPI_ERR = MPI_Send(&rank, 1, MPI_INT, MASTER_ID, TAG_INIT, MPI_COMM_WORLD);
		MPI_ERR = MPI_Send(pic, procHeight * disp_width, MPI_INT, MASTER_ID, TAG_DATA, MPI_COMM_WORLD);
		gettimeofday(&commt2, NULL);
		commTotal += getTime(rank, commt1, commt2);
	/* ****************** End of WORKER ****************** */
	}

	MPI_Finalize();
	
	/*------ Time End --------*/
	gettimeofday(&t2, NULL);
	grandTotal += getTime(rank, t1, t2);

	getSecsUsecs(ioTotal, &ioTotalSecs, &ioTotalUSecs);
	printf("Rank = %d, I/O TIME = %ld.%ld\n", rank, ioTotalSecs, ioTotalUSecs);

	getSecsUsecs(commTotal, &commTotalSecs, &commTotalUSecs);
	printf("Rank = %d, COMM TIME = %ld.%ld\n", rank, commTotalSecs, commTotalUSecs);

	getSecsUsecs(compTotal, &compTotalSecs, &compTotalUSecs);
	printf("Rank = %d, COMP TIME = %ld.%ld\n", rank, compTotalSecs, compTotalUSecs);

	getSecsUsecs(grandTotal, &grandTotalSecs, &grandTotalUSecs);
	printf("Rank = %d, TOTAL TIME = %ld.%ld\n", rank, grandTotalSecs, grandTotalUSecs);
	printf("\n");
	/*------------------------*/
}
