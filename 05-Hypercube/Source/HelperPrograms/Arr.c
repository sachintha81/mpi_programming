/***********************************************************************************/
//	Array Size
//	Example
/***********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdarg.h>
#include <sys/time.h>

int main(int argc, char *argv[]) {
	int* arr2;
	int i;
	int len;

	arr2 = malloc(sizeof(int) * 20);

	int arr[10];
	
	for(i = 0; i < 5; i++) {
		arr[i] = i;
		printf("arr[%d] = %d\n", i, arr[i]);
	}

	len = sizeof(arr)/sizeof(arr[0]);
	printf("Size = %d\n", len);

	return 0;
}
