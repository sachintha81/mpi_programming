/***********************************************************************************/
//	CS789 - Parallel Programming
//	Sachintha Gurudeniya
//	11/29/2015
//	Assignment #6 - Hypercube Routing (Parallel)
//	Version : Q16
/***********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "mpi.h"

#define	SOURCE			0
#define	DEST			1
#define	MSGNO			2
#define	TYPE			3
#define	INIT_TYPE		4
#define	ORGSOURCE		5
#define	HOPCNT			6
#define	MSGSIZE			25

// Message Types
#define	MSG				0
#define ACK				1
#define	DONE			2
#define	STOP			3
#define	PRINT 			4
#define	TREE			5
#define	TREEDONE		6
#define	PARENT			7
#define	PARENTFOUND		8
#define	FORBCAST		9
#define	BCAST			10
#define	FORBCASTACK		11
#define	BCASTACK		12
#define	REDUCEFORWARD	13
#define	REDUCEBACKWARD	14

#define INITDONE		16

#define TAG				11111

#define PRINT_MGR		0
#define	NODE_ZERO		0

const unsigned int power2[12] = {1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048};
int MASTER;
int ROOT;
int MsgCounter = 0;

char *getMsgType(int type) 
{
	char* str = (char*)malloc(sizeof(char*) * 8);
	switch (type) {
		case MSG:
			strcpy(str, "MSG");
			break;
		case ACK:
			strcpy(str, "ACK");
			break;
		case DONE:
			strcpy(str, "DONE");
			break;
		case STOP:
			strcpy(str, "STOP");
			break;
		case PRINT:
			strcpy(str, "PRINT");
			break;
	}
	return str;
}

unsigned int compute_next_dest(unsigned int rank, unsigned int dest)
{
	if (rank == dest)
	{
		return dest;
	}

	unsigned int m = rank ^ dest;
	int c = 0;

	// Find the first "1" searching the string from right to left
	while(m % 2 == 0) {	// While m is even
		c++;
		m >>= 1;
	}

	return power2[c] ^ rank;
}

char *byte2bin(unsigned int x, int dim)
{
	int z;
	char* b = (char*)malloc(sizeof(char*) * (dim));

	b[0] = '\0';
	for (z = power2[dim-1]; z > 0; z >>= 1) {
		strcat(b, ((x & z) == z) ? "1" : "0");
	}

	return b;
}

int FlipBits(int num, int bitPos)
{
	return num ^ 1 << bitPos;
}

int CheckBit(int num, int bit)
{
	return (num >> bit) & 1;
}

int parity(int n)
{
	int c = 0;
	while (n > 0) {
		if (n%2 == 1)
			c++;
		n >>= 1;
	}
	return c;
}

int findFirst(int i)
{
	int c = 0;
	while (i % 2 == 0) {
		c++;
		i >>= 1;
	}
	return c;
}

int myPrev(int num, int dim)
{
	int prev;
	int ones;
	int bitToFlip;

	/* myAddress is 000...000 */
	if (num == 0) {
		prev = pow(2, dim-1);
	}
	else {
		ones = parity(num);

		/* Odd # of 1s */
		if (ones % 2 == 1) {
			/* Flip LSB */
			bitToFlip = 0;
		}
		/* Even # of 1s */
		else {
			/* Flip bit to the left of rightmost 1 */
			bitToFlip = findFirst(num) + 1;
		}
		prev = num ^ 1 << bitToFlip;
	}

	return prev;
}

int myNext(int num, int dim)
{
	int next;
	int ones;
	int bitToFlip;

	/* myAddress is 100...000 */
	if (num == pow(2, dim-1)) {
		next = 0;
	}
	else {
		ones = parity(num);
		
		/* Odd # of 1s */
		if (ones % 2 == 1) {
			/* Flip bit to the left of rightmost 1 */
			bitToFlip = findFirst(num) + 1;
		}
		/* Even # of 1s */
		else {
			/* Flip LSB */
			bitToFlip = 0;
		}
		next = num ^ 1 << bitToFlip;
	}

	return next;
}

int RandomNumber(int max)
{
	int i, num;
	time_t t;

	/* Intializes random number generator */
	struct timeval tm;
	gettimeofday(&tm, NULL);
	srandom(tm.tv_sec + tm.tv_usec * 1000000ul);

	num = random() % max;

	return num;
}

void CreateMessage(int *msg, int source, int dest, int type, int msgno)
{
	msg[SOURCE] = source;
	msg[DEST] = dest;
	msg[TYPE] = type;
	msg[INIT_TYPE] = type;
	msg[HOPCNT] = 0;

	if (msgno > 0) {
		msg[MSGNO] = msgno;
	}
	else {
		if (type != ACK) {
			msg[MSGNO] = source * 100 + MsgCounter;
			MsgCounter++;
		}
		else {
			msg[MSGNO] = msgno;
		}
	}
}

void PrintMessage(int *msg, int rank, int dim)
{
	int i;
	int hops = 0;
	char path[256];
	int sourceIndex = HOPCNT + 1;
	int destIndex = SOURCE;

	//	Printing SOURCE
	strcpy(path, "");

	//	Printing intermediate nodes
	if (msg[HOPCNT] > 0) {
		for (i = 0; i < msg[HOPCNT]; i++) {
			if (msg[HOPCNT + i + 1] != msg[destIndex])
			{
				strcat(path, byte2bin(msg[HOPCNT + i + 1], dim));
				strcat(path, "->");
				hops++;
			}
		}
	}
	
	if (hops == 0) {
		strcat(path, byte2bin(msg[HOPCNT+1], dim));
		strcat(path, "->");
	}
	//	Printing DEST
	strcat(path, byte2bin(msg[destIndex], dim));

	printf("%d(%s):\t  Message (%s) #%04d from %d(%s) to %d(%s) in %d hops (%s)\n", 
		rank, 
		byte2bin(rank, dim), 
		getMsgType(msg[INIT_TYPE]), 
		msg[MSGNO], 
		msg[sourceIndex], 
		byte2bin(msg[sourceIndex], dim), 
		msg[destIndex], 
		byte2bin(msg[destIndex], dim), 
		hops, 
		path);
}

void PrintChildren(int* list, int count, int rank)
{
	int i;
	char str[80];
	char s[10];
	strcpy(str, "");
	for (i = 0; i < count; i++) {
		sprintf(s, "%d", list[i]);
		strcat(str, s);
		strcat(str, "  ");
	}
	printf("Rank = %d, Children = %s\n", rank, str);
}

int main(int argc, char *argv[])
{
	MPI_Status status;
	MPI_Comm comm;
	int flag;
	int MPI_ERR;
	int rank, size;
	int dim, numMsgs;
	int* Msg;
	int* ChildList;
	int i, rnd, next, prev;
	int sentCount, ackCount, stopCount, childCount, parentCount;
	int countForBC, countBC;
	int saveNum, saveSrc, saveType;
	int parent, neighbor, treedone;
	int reduceFwd, reduceBwd;

	if (argc != 3) {
		printf("Usage: Hypercube <Dim> <Num of Msgs>\n");
		printf("\t Dim \t = Number of Dimensions\n");
		printf("\t Num of Msgs \t = Number of Messages to be sent\n");
		printf("%d args supplied\n", argc);
		exit(0);
	}

	MPI_ERR = MPI_Init(&argc, &argv);
	MPI_ERR = MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_ERR = MPI_Comm_size(MPI_COMM_WORLD, &size);

	MASTER	= size - 1;
	ROOT	= size - 2;
	dim		= atoi(argv[1]);
	numMsgs	= atoi(argv[2]);

	Msg = (int *)calloc(MSGSIZE, sizeof(int));

	if (rank == MASTER) {
		//	MASTER CODE
		int count = 0;
		treedone = 0;

		CreateMessage(Msg, rank, NODE_ZERO, TREE, 0);
		MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, NODE_ZERO, TAG, MPI_COMM_WORLD);
		
		while (1) {
			MPI_ERR = MPI_Recv(&Msg[0], MSGSIZE, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			if (Msg[TYPE] == TREEDONE && treedone == 0) {
				treedone = 1;
				CreateMessage(Msg, rank, NODE_ZERO, INITDONE, 0);
				MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, NODE_ZERO, TAG, MPI_COMM_WORLD);				
			}
			if (Msg[TYPE] == DONE) {
				count++;
			}
			else if(Msg[TYPE] == PRINT) {
				// PrintMessage(Msg, Msg[SOURCE], dim);
			}
			if (count == size-1 && treedone == 1) {
				break;
			}
		}

		CreateMessage(Msg, rank, NODE_ZERO, STOP, 0);
		MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, NODE_ZERO, TAG, MPI_COMM_WORLD);

		free(Msg);
	}
	else {
		//	SLAVE CODE
		ChildList = (int *)calloc(dim, sizeof(int));
		for (i = 0; i < dim; i++) {
			ChildList[i] = -1;
		}

		treedone 	= 0;
		sentCount 	= 0;
		ackCount 	= 0;
		stopCount 	= ROOT;
		childCount 	= 0;
		parentCount	= 0;
		parent 		= -1;
		countForBC	= 0;
		countBC 	= 0;
		reduceFwd 	= 0;
		reduceBwd 	= 0;

		while(1) {
			MPI_ERR = MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &status);

			//	If there is a message to be received
			if (flag == 1) {
				MPI_ERR = MPI_Recv(&Msg[0], MSGSIZE, MPI_INT, status.MPI_SOURCE, TAG, MPI_COMM_WORLD, &status);
				
				if (Msg[TYPE] == INITDONE) {
					if (status.MPI_SOURCE == MASTER) {
						for (i = 0; i < size-1; i++) {
							CreateMessage(Msg, rank, i, INITDONE, 0);
							next = compute_next_dest(rank, Msg[DEST]);
							MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);
						}
					}
					else {
						if (Msg[DEST] != rank) {
							next = compute_next_dest(rank, Msg[DEST]);
							MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);
						}
						else {
							treedone = 1;
						}
					}
				}
				//	Spanning Tree
				if (Msg[TYPE] == TREE) {
					if (status.MPI_SOURCE == MASTER) {
						for (i = 0; i < size-1; i++) {
							if (CheckBit(i, dim-1) == 0) {
								CreateMessage(Msg, rank, i, TREE, 0);
								next = compute_next_dest(rank, Msg[DEST]);
								MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);
							}
						}
					}
					else {
						if (Msg[DEST] != rank) {
							next = compute_next_dest(rank, Msg[DEST]);
							MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);
						}
						else {
							// Spanning Tree construction
							int max = rank;
							for(i = 0; i < dim; i++) {
								neighbor = FlipBits(rank, i);
								if (neighbor > max) {
									max = neighbor;
								}
							}
							parent = max;
							if (rank != ROOT) {
								CreateMessage(Msg, rank, parent, PARENT, 0);
								MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, parent, TAG, MPI_COMM_WORLD);
							}
						}
					}					
				}
				else if (Msg[TYPE] == PARENT) {
					ChildList[childCount] = Msg[SOURCE];
					childCount++;
					if (rank != ROOT) {
						if (parent < 0) {
							int max = rank;
							for(i = 0; i < dim; i++) {
								neighbor = FlipBits(rank, i);
								if (neighbor > max) {
									max = neighbor;
								}
							}
							parent = max;
							if (rank != ROOT) {
								CreateMessage(Msg, rank, parent, PARENT, 0);
								MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, parent, TAG, MPI_COMM_WORLD);
							}
						}
					}
					else {
						if (childCount == (pow(2, dim-1) - 1)) {
							CreateMessage(Msg, rank, NODE_ZERO, TREEDONE, 0);
							next = compute_next_dest(rank, Msg[DEST]);
							MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);
						}
					}
				}
				else if(Msg[TYPE] == TREEDONE) {
					if (Msg[DEST] != rank) {
						next = compute_next_dest(rank, Msg[DEST]);
						MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);
					}
					//	At NODE_ZERO, when tree is done, send the message to MASTER 
					else {
						treedone = 1;
						MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, MASTER, TAG, MPI_COMM_WORLD);
					}
				}

				if (Msg[TYPE] == FORBCAST) {
					if (Msg[DEST] != rank) {
						next = compute_next_dest(rank, Msg[DEST]);
						MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);
					}
					else {
						//printf("FORBCAST: @%d received From %d\n", rank, status.MPI_SOURCE);
						int sender = Msg[SOURCE];
						CreateMessage(Msg, rank, sender, FORBCASTACK, 0);
						next = compute_next_dest(rank, Msg[DEST]);
						MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);
						for (i = 0; i < childCount; i++) {
							CreateMessage(Msg, rank, ChildList[i], BCAST, 0);
							MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, Msg[DEST], TAG, MPI_COMM_WORLD);
							countBC++;
						}
					}
				}
				else if (Msg[TYPE] == BCAST) {
					//printf("BROADCAST: @%d received From %d\n", rank, status.MPI_SOURCE);
					int sender = Msg[SOURCE];
					CreateMessage(Msg, rank, sender, BCASTACK, 0);
					MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, sender, TAG, MPI_COMM_WORLD);
					for (i = 0; i < childCount; i++) {
						CreateMessage(Msg, rank, ChildList[i], BCAST, 0);
						MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, Msg[DEST], TAG, MPI_COMM_WORLD);
						countBC++;
					}
				}
				else if (Msg[TYPE] == FORBCASTACK) {
					if (Msg[DEST] != rank) {
						next = compute_next_dest(rank, Msg[DEST]);
						MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);
					}
					else {
						countForBC--;
					}
				}
				else if (Msg[TYPE] == BCASTACK) {
					if (Msg[DEST] != rank) {
						next = compute_next_dest(rank, Msg[DEST]);
						MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);
					}
					else {
						countBC--;
					}
				}

				if (Msg[TYPE] == REDUCEFORWARD) {
					if (Msg[ORGSOURCE] == rank) {
						printf("REDUCEFORWARD returned to %d. From %d.\n", rank, Msg[SOURCE]);
						reduceFwd--;
					}
					else {
						next = myNext(rank, dim);
						printf("REDUCEFORWARD @ %d. From %d to %d.\n", rank, Msg[SOURCE], next);
						int orgSource = Msg[ORGSOURCE];
						CreateMessage(Msg, rank, next, REDUCEFORWARD, 0);
						Msg[ORGSOURCE] = orgSource;
						MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);
					}
				}
				if (Msg[TYPE] == REDUCEBACKWARD) {
					if (Msg[ORGSOURCE] == rank) {
						printf("REDUCEBACKWARD returned to %d. From %d.\n", rank, Msg[SOURCE]);
						reduceBwd--;
					}
					else {
						prev = myPrev(rank, dim);
						printf("REDUCEBACKWARD @ %d. From %d to %d.\n", rank, Msg[SOURCE], prev);
						int orgSource = Msg[ORGSOURCE];
						CreateMessage(Msg, rank, prev, REDUCEBACKWARD, 0);
						Msg[ORGSOURCE] = orgSource;
						MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, prev, TAG, MPI_COMM_WORLD);
					}
				}

				//	Termination using STOP messages
				if (Msg[TYPE] == STOP) {
					next = myNext(rank, dim);
					CreateMessage(Msg, rank, next, STOP, 0);
					MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);
					printf("Terminating @ %d. Sent to %d.\n", rank, next);
					break;
				}

				//	If a DONE message received
				if (Msg[TYPE] == DONE) {
					//	If you're the NODE_ZERO, send it to the MASTER
					if (rank == NODE_ZERO) {
						MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, MASTER, TAG, MPI_COMM_WORLD);
					}
					//	Otherwise just route
					else {
						next = compute_next_dest(rank, Msg[DEST]);
						MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);
					}					
				}

				// If the message type is PRINT
				if (Msg[TYPE] == PRINT) {
					//	@ SlaveID = 0, send to MASTER
					if (rank == PRINT_MGR) {
						MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, MASTER, TAG, MPI_COMM_WORLD);
					//	@ Other slaves, route it to SlaveID = 0 
					} else {
						next = compute_next_dest(rank, Msg[DEST]);
						MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);
					}
				}
				else if (Msg[TYPE] == MSG || Msg[TYPE] == ACK) {
					if (Msg[DEST] == rank) {
						if (Msg[TYPE] == ACK) {
							ackCount++;
						}

						saveType = Msg[TYPE];
						if (Msg[TYPE] == MSG) {
							saveSrc = Msg[SOURCE];
							saveNum = Msg[MSGNO];							
						}

						//	Sending messagest to be printed to Slave 0
						Msg[TYPE] = PRINT;
						Msg[SOURCE] = rank;
						Msg[DEST] = PRINT_MGR;
						next = compute_next_dest(rank, Msg[DEST]);
						MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);
		
						//	If a MSG message arrived at its intended destination
						//	an ACK is created and sent back
						if (saveType == MSG) {
							CreateMessage(Msg, rank, saveSrc, ACK, saveNum);
							Msg[HOPCNT] = 1;
							Msg[HOPCNT + 1] = rank;
							next = compute_next_dest(rank, Msg[DEST]);
							MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);					
						}
					}
					else {
						//	Intermediate routing of MSG messages
						Msg[HOPCNT] = Msg[HOPCNT] + 1;
						Msg[HOPCNT + Msg[HOPCNT]] = rank;
						next = compute_next_dest(rank, Msg[DEST]);
						MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);
					}
				}
				//	Sending DONE messages routed to Slave 0.
				//	Sent when a slave gets the same number of messages as it has sent
				if (ackCount == numMsgs && countForBC == 0 && countBC == 0 && reduceFwd == 0 && reduceBwd == 0) {
					CreateMessage(Msg, rank, NODE_ZERO, DONE, 0);
					next = compute_next_dest(rank, Msg[DEST]);
					MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);
					ackCount++;
				}
			}
			else {
				//	Sending original outgoing MSG messages
				if (treedone == 1) {
					if (sentCount < numMsgs) {
						rnd = RandomNumber(size-1);
						CreateMessage(Msg, rank, rnd, MSG, 0);
						Msg[HOPCNT] = 1;
						Msg[HOPCNT + 1] = rank;
						next = compute_next_dest(rank, Msg[DEST]);
						MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);
						sentCount++;
					}
					if (sentCount == numMsgs && rank == NODE_ZERO) {
						CreateMessage(Msg, rank, ROOT, FORBCAST, 0);
						Msg[HOPCNT] = 1;
						Msg[HOPCNT + 1] = rank;
						next = compute_next_dest(rank, Msg[DEST]);
						MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);
						sentCount++;
						countForBC++;

						next = myNext(rank, dim);
						CreateMessage(Msg, rank, next, REDUCEFORWARD, 0);
						Msg[ORGSOURCE] = rank;
						Msg[HOPCNT] = 1;
						Msg[HOPCNT + 1] = rank;
						MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);
						reduceFwd++;
						printf("REDUCEFORWARD originated at %d. Sent to %d.\n", rank, next);

						prev = myPrev(rank, dim);
						CreateMessage(Msg, rank, prev, REDUCEBACKWARD, 0);
						Msg[ORGSOURCE] = rank;
						Msg[HOPCNT] = 1;
						Msg[HOPCNT + 1] = rank;
						MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, prev, TAG, MPI_COMM_WORLD);
						reduceBwd++;
						printf("REDUCEBACKWARD originated at %d. Sent to %d.\n", rank, prev);
					}
				}
			}
		}

		free(ChildList);
		free(Msg);
	}

	MPI_ERR = MPI_Finalize();
	return 0;
}
