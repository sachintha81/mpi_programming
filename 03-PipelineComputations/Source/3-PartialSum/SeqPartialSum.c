/***********************************************************************************/
//	CS789 - Parallel Programming
//	Assignment #4 - System of Equations
//	Sachintha Gurudeniya
//	11/4/2015
/***********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdarg.h>

int main(int argc, char *argv[]) {
	int n;
	long int *sum, *x;
	int i, j, count;

	if (argc != 2) {
		printf("Usage: PartialSum <n>\n");
		printf("\t n \t = Number of items.\n");
		printf("%d args supplied\n", argc);
		exit(0);
	}

	/* Get variables from commando line */
	n   = atoi(argv[1]);	// Number of items.

	sum = (long int *)calloc(n, sizeof(long int));
	x = (long int *)calloc(n, sizeof(long int));

	for (i = 0; i < n; i++) {
		x[i] = i + 1;
	}

	count = 0;
	sum[0] = x[0];
	for (i = 1; i < n; i++) {
		sum[i] = sum[i-1] + x[i];
		count++;
	}

	printf("Sum = %ld\n", sum[count]);
	return 0;
}
