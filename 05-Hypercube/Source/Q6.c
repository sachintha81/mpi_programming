/***********************************************************************************/
//	CS789 - Parallel Programming
//	Sachintha Gurudeniya
//	11/29/2015
//	Assignment #6 - Hypercube Routing (Parallel)
//	Version : Q6
/***********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "mpi.h"

#define	SOURCE		0
#define	DEST		1
#define	MSGNO		2
#define	TYPE		3
#define	INIT_TYPE	4
#define	HOPCNT		5
#define	MSGSIZE		25

// Message Types
#define	MSG			0
#define ACK			1
#define	DONE		2
#define	STOP		3
#define	PRINT 		4

#define TAG			11111

#define PRINT_MGR	0

const unsigned int power2[12] = {1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048};
int MASTER;
int MsgCounter = 0;

char *getMsgType(int type) {
	char* str = (char*)malloc(sizeof(char*) * 8);
	switch (type) {
		case MSG:
			strcpy(str, "MSG");
			break;
		case ACK:
			strcpy(str, "ACK");
			break;
		case DONE:
			strcpy(str, "DONE");
			break;
		case STOP:
			strcpy(str, "STOP");
			break;
		case PRINT:
			strcpy(str, "PRINT");
			break;
	}
	return str;
}

unsigned int compute_next_dest(unsigned int rank, unsigned int dest)
{
	if (rank == dest)
	{
		return dest;
	}

	unsigned int m = rank ^ dest;
	int c = 0;

	// Find the first "1" searching the string from right to left
	while(m % 2 == 0) {	// While m is even
		c++;
		m >>= 1;
	}

	return power2[c] ^ rank;
}

char *byte2bin(unsigned int x, int dim)
{
	int z;
	char* b = (char*)malloc(sizeof(char*) * (dim));

	b[0] = '\0';
	for (z = power2[dim-1]; z > 0; z >>= 1) {
		strcat(b, ((x & z) == z) ? "1" : "0");
	}

	return b;
}

int RandomNumber(int max)
{
	int i, num;
	time_t t;

	/* Intializes random number generator */
	struct timeval tm;
	gettimeofday(&tm, NULL);
	srandom(tm.tv_sec + tm.tv_usec * 1000000ul);

	num = random() % max;

	return num;
}

void CreateMessage(int *msg, int source, int dest, int type, int msgno)
{
	msg[SOURCE] = source;
	msg[DEST] = dest;
	msg[TYPE] = type;
	msg[INIT_TYPE] = type;
	msg[HOPCNT] = 0;

	if (msgno > 0) {
		msg[MSGNO] = msgno;
	}
	else {
		if (type != ACK) {
			msg[MSGNO] = source * 100 + MsgCounter;
			MsgCounter++;
		}
		else {
			msg[MSGNO] = msgno;
		}
	}
}

void PrintMessage(int *msg, int rank, int dim)
{
	int i;
	int hops = 0;
	char path[256];
	int sourceIndex = HOPCNT + 1;
	int destIndex = SOURCE;

	//	Printing SOURCE
	strcpy(path, "");

	//	Printing intermediate nodes
	if (msg[HOPCNT] > 0) {
		for (i = 0; i < msg[HOPCNT]; i++) {
			if (msg[HOPCNT + i + 1] != msg[destIndex])
			{
				strcat(path, byte2bin(msg[HOPCNT + i + 1], dim));
				strcat(path, "->");
				hops++;
			}
		}
	}
	
	//	Printing DEST
	strcat(path, byte2bin(msg[destIndex], dim));

	printf("%d(%s):\t  Message (%s) #%04d from %d(%s) to %d(%s) in %d hops (%s)\n", 
		rank, 
		byte2bin(rank, dim), 
		getMsgType(msg[INIT_TYPE]), 
		msg[MSGNO], 
		msg[sourceIndex], 
		byte2bin(msg[sourceIndex], dim), 
		msg[destIndex], 
		byte2bin(msg[destIndex], dim), 
		hops, 
		path);
}

int main(int argc, char *argv[])
{
	MPI_Status status;
	MPI_Comm comm;
	int flag;
	int MPI_ERR;
	int rank, size;
	int dim, numMsgs;
	int* Msg;
	int i, rnd, next, sentCount, ackCount;
	int saveNum, saveSrc, saveType;

	if (argc != 3) {
		printf("Usage: Hypercube <Dim> <Num of Msgs>\n");
		printf("\t Dim \t = Number of Dimensions\n");
		printf("\t Num of Msgs \t = Number of Messages to be sent\n");
		printf("%d args supplied\n", argc);
		exit(0);
	}

	MPI_ERR = MPI_Init(&argc, &argv);
	MPI_ERR = MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_ERR = MPI_Comm_size(MPI_COMM_WORLD, &size);

	MASTER	= size - 1;
	dim		= atoi(argv[1]);
	numMsgs	= atoi(argv[2]);

	Msg = (int *)calloc(MSGSIZE, sizeof(int));

	if (rank == MASTER) {
		//	MASTER CODE
		int count = 0;

		while (1) {
			MPI_ERR = MPI_Recv(&Msg[0], MSGSIZE, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			if (Msg[TYPE] == DONE) {
				count++;
			}
			else if(Msg[TYPE] == PRINT) {
				PrintMessage(Msg, Msg[SOURCE], dim);
			}
			if (count == size-1) {
				break;
			}
		}

		for (i = 0; i < size-1; i++) {
			CreateMessage(Msg, rank, i, STOP, 0);
			MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, i, TAG, MPI_COMM_WORLD);
		}
	}
	else {
		//	SLAVE CODE
		sentCount = 0;
		ackCount = 0;

		while(1) {
			MPI_ERR = MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &status);
			if (flag == 1) {
				MPI_ERR = MPI_Recv(&Msg[0], MSGSIZE, MPI_INT, status.MPI_SOURCE, TAG, MPI_COMM_WORLD, &status);
				if (Msg[TYPE] == STOP) {
					break;
				}

				// If the message type is PRINT
				if (Msg[TYPE] == PRINT) {
					//	@ SlaveID = 0, send to MASTER
					if (rank == PRINT_MGR) {
						MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, MASTER, TAG, MPI_COMM_WORLD);
					//	@ Other slaves, route it to SlaveID = 0 
					} else {
						next = compute_next_dest(rank, Msg[DEST]);
						MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);
					}
				}
				else {
					if (Msg[DEST] == rank) {
						if (Msg[TYPE] == ACK) {
							ackCount++;
						}

						saveType = Msg[TYPE];
						if (Msg[TYPE] == MSG) {
							saveSrc = Msg[SOURCE];
							saveNum = Msg[MSGNO];							
						}

						//	Sending messagest to be printed to Slave 0
						Msg[TYPE] = PRINT;
						Msg[SOURCE] = rank;
						Msg[DEST] = PRINT_MGR;
						next = compute_next_dest(rank, Msg[DEST]);
						MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);
		
						//	If a MSG message arrived at its intended destination
						//	an ACK is created and sent back
						if (saveType == MSG) {
							CreateMessage(Msg, rank, saveSrc, ACK, saveNum);
							Msg[HOPCNT] = 1;
							Msg[HOPCNT + 1] = rank;
							next = compute_next_dest(rank, Msg[DEST]);
							MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);					
						}
					}
					else {
						//	Intermediate routing of MSG messages
						Msg[HOPCNT] = Msg[HOPCNT] + 1;
						Msg[HOPCNT + Msg[HOPCNT]] = rank;
						next = compute_next_dest(rank, Msg[DEST]);
						MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);
					}

					//	DONE messages directly to MASTER
					//	Sent when a slave gets the same number of messages as it has sent
					if (ackCount == numMsgs) {
						CreateMessage(&Msg[0], rank, MASTER, DONE, 0);
						MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, MASTER, TAG, MPI_COMM_WORLD);
						ackCount++;
					}
				}
			}
			else {
				//	Sending original outgoing MSG messages
				if (sentCount < numMsgs) {
					rnd = RandomNumber(size-1);
					CreateMessage(Msg, rank, rnd, MSG, 0);
					Msg[HOPCNT] = 1;
					Msg[HOPCNT + 1] = rank;
					next = compute_next_dest(rank, Msg[DEST]);
					MPI_ERR = MPI_Send(&Msg[0], MSGSIZE, MPI_INT, next, TAG, MPI_COMM_WORLD);
					sentCount++;
				}
			}
		}
	}

	MPI_ERR = MPI_Finalize();
	return 0;
}
